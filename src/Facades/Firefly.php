<?php

namespace Firefly\Facades;

use Illuminate\Support\Facades\Facade;
use Firefly\Contracts\Factory;

/**
 * @see \Firefly\FireflyManager
 */
class Firefly extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return Factory::class;
    }
}
