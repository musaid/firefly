<?php

namespace Firefly\Contracts;

interface Factory
{
    /**
     * Get an OAuth provider implementation.
     *
     * @param  string  $driver
     * @return \Firefly\Contracts\Provider
     */
    public function driver($driver = null);
}
