<?php

namespace Firefly\Providers;

use InvalidArgumentException;

class InvalidStateException extends InvalidArgumentException
{
    //
}
